#include "Grille.hpp"

using namespace std;

Grille::Grille()
{
}

Grille::Grille(int width, int length)
{
    this->longueur = length;
    this->largeur = width;
    this->grille.resize(width, std::vector<int>(length));
}

void Grille::AfficherGrille()
{
    cout << "---------Grille---------" << endl;
    for (long unsigned int i = 0; i < largeur; i++)
    {
        cout << "      ";
        for (long unsigned int j = 0; j < longueur; j++)
        {
            if (j != 0)
                cout << "  |  ";
            if (grille[i][j] == 1)
                cout << "X";
            else if (grille[i][j] == 2)
                cout << "O";
            else if (grille[i][j] == 0)
                cout << "-";
        }
        cout << endl;
    }
}

void Grille::Remplir(int w, int l, int symb)
{
    grille[w-1][l-1] = symb;
}


std::vector<std::vector<int>> Grille::getGrille()
{
    return this->grille;
}

int Grille::getLongueur()
{
    return this->longueur;
}

int Grille::getLargeur()
{
    return this->largeur;
}