#include <iostream>
#include <string>
#include "Morpion.hpp"
#include "Grille.hpp"

using namespace std;

Morpion::Morpion()
{
    this->grille = Grille(3, 3);
}
void Morpion::Lance()
{
    InitJoueur();
    Jeu();
}
bool Morpion::VerifVictoireLigne(Player player)
{
    int compteur;
    for (int i = 0; i < grille.getLargeur(); i++)
    {
        compteur = 0;
        for (int j = 0; j < grille.getLongueur(); j++)
        {
            if (grille.getGrille()[i][j] == player.getSymbole())
            {
                compteur++;
            }
        }
        if (compteur == 3)
        {
            return true;
        }
    }
    return false;
}
bool Morpion::VerifVictoireColonne(Player player)
{
    int compteur;
    for (int i = 0; i < grille.getLargeur(); i++)
    {
        compteur = 0;
        for (int j = 0; j < grille.getLongueur(); j++)
        {
            if (grille.getGrille()[j][i] == player.getSymbole())
            {
                compteur++;
            }
        }
        if (compteur == 3)
        {
            return true;
        }
    }
    return false;
}
bool Morpion::VerifVictoireDiagonal(Player player)
{
    if (grille.getGrille()[0][0] == player.getSymbole() && grille.getGrille()[1][1] == player.getSymbole() && grille.getGrille()[2][2] == player.getSymbole())
        return true;
    else if (grille.getGrille()[2][0] == player.getSymbole() && grille.getGrille()[1][1] == player.getSymbole() && grille.getGrille()[0][2] == player.getSymbole())
        return true;
    else
        return false;
}

void Morpion::Tour(Player player)
{
    int inputX;
    int inputY;
    cout << player.getName() << " entre le numéro de la ligne et la colonne (X Y)" << endl;
    cin >> inputX >> inputY;

    while (inputX > grille.getLargeur() || inputX < 1 || inputY > grille.getLongueur() || inputY < 1)
    {
        cout << "Hors grille" << endl;
        cout << "Entrez le numéro de la ligne et la colonne (X Y)" << endl;
        cin >> inputX >> inputY;
    }
    while(grille.getGrille()[inputX-1][inputY-1]!=0)
    {
        cout << "Case déjà occupé" << endl;
        cout << "Entrez le numéro de la ligne et la colonne (X Y)" << endl;
        cin >> inputX >> inputY;
    }
    grille.Remplir(inputX, inputY, player.getSymbole());
}

void Morpion::InitJoueur()
{
    string temp;
    cout << "Joueur 1 : choisie ton nom " << endl;
    cin >> temp;
    player1.setName(temp);
    player1.setSymbole(1);
    cout << "Joueur 2 : choisie ton nom " << endl;
    cin >> temp;
    player2.setName(temp);
    player2.setSymbole(2);
}
void Morpion::Jeu()
{
    currentPlayer = player1;
    while (!VerifVictoire(currentPlayer) && !Egalite())
    {
        if (currentPlayer.getSymbole() == player1.getSymbole())
            currentPlayer = player2;
        else
            currentPlayer = player1;
        grille.AfficherGrille();
        Tour(currentPlayer);
    }
    if (VerifVictoire(currentPlayer) == true)
    {
        cout << "Victoire de " << currentPlayer.getName() << endl;
        grille.AfficherGrille();
    }
    else
    {
        cout << "Egalité" << endl;
        grille.AfficherGrille();
    }
}
bool Morpion::Egalite()
{
    int compteur = 0;
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[i][j] != 0)
            {
                compteur++;
            }
        }
    }
    if (compteur == 9)
        return true;
    else
        return false;
}
bool Morpion::VerifVictoire(Player player)
{
    if (VerifVictoireLigne(player) || VerifVictoireColonne(player) || VerifVictoireDiagonal(player))
    {
        cout << VerifVictoireLigne(player) << " " << VerifVictoireColonne(player) << " " << VerifVictoireDiagonal(player) << endl;
        return true;
    }
    else
        return false;
}