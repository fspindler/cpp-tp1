#include <iostream>
#include <string>
#include "Puissance4.hpp"
#include "Grille.hpp"

using namespace std;

Puissance4::Puissance4()
{
    this->grille = Grille(4, 7);
}
void Puissance4::Lance()
{
    InitJoueur();
    Jeu();
}
bool Puissance4::VerifVictoireLigne(Player player)
{
    int compteur;
    bool result = false;
    for (int i = 0; i < grille.getLargeur(); i++)
    {
        compteur = 0;
        for (int j = 0; j < grille.getLongueur(); j++)
        {
            if (grille.getGrille()[i][j] == player.getSymbole())
            {
                compteur++;
            }
            else if (compteur == 4)
            {
                result = true;
            }
            else
            {
                compteur = 0;
            }
        }
        
    }
    return result;
}
bool Puissance4::VerifVictoireColonne(Player player)
{
    int compteur;
    for (int i = 0; i < grille.getLargeur(); i++)
    {
        compteur = 0;
        for (int j = 0; j < grille.getLongueur(); j++)
        {
            if (grille.getGrille()[j][i] == player.getSymbole())
            {
                compteur++;
            }
        }
        if (compteur == 4)
        {
            return true;
        }
    }
    return false;
}
bool Puissance4::VerifVictoireDiagonal(Player player)
{
   return false;
}

void Puissance4::Tour(Player player)
{
    //int inputX;
    int inputY;
    cout << player.getName() << " entre le numéro la colonne " << endl;
    cin >> inputY;

    while (inputY > grille.getLongueur() || inputY < 1)
    {
        cout << "Hors grille" << endl;
        cout << "Entrez le numéro de la colonne" << endl;
        cin >> inputY;
    }

    for (int i=grille.getLargeur();i>0;i--)
    {

        if(grille.getGrille()[i][inputY]==0)
        {
            cout<<i<<endl;
            grille.Remplir(i, inputY, player.getSymbole());
            break;
        }
    }
    // while(grille.getGrille()[inputX-1][inputY-1]!=0)
    // {
    //     cout << "Case déjà occupé" << endl;
    //     cout << "Entrez le numéro de la ligne et la colonne (X Y)" << endl;
    //     cin >> inputX >> inputY;
    // }
    //grille.Remplir(inputX, inputY, player.getSymbole());
}

void Puissance4::InitJoueur()
{
    string temp;
    cout << "Joueur 1 : choisie ton nom " << endl;
    cin >> temp;
    player1.setName(temp);
    player1.setSymbole(1);
    cout << "Joueur 2 : choisie ton nom " << endl;
    cin >> temp;
    player2.setName(temp);
    player2.setSymbole(2);
}
void Puissance4::Jeu()
{
    //currentPlayer = player1;
    Tour(currentPlayer);
    // while (!VerifVictoire(currentPlayer) || !Egalite())
    // {
    //     if (currentPlayer.getSymbole() == player1.getSymbole())
    //         currentPlayer = player2;
    //     else
    //         currentPlayer = player1;
    //     //grille.AfficherGrille();
    //     //Tour(currentPlayer);
    // }
    // if (VerifVictoire(currentPlayer) == true) 
    // {
    //     cout << "Victoire de " << currentPlayer.getName() << endl;
    //     grille.AfficherGrille();
    // }
    // else
    // {
    //     cout << "Egalité" << endl;
    //     grille.AfficherGrille();
    // }
}
bool Puissance4::Egalite()
{
    bool res = false;
    for (long unsigned int i = 0; i < grille.getGrille().size(); i++)
    {
        for (long unsigned int j = 0; j < grille.getGrille()[i].size(); j++)
        {
            if (grille.getGrille()[i][j] != 0)
            {
                res = true;
            }
        }
    }
    return res;
}
bool Puissance4::VerifVictoire(Player player)
{
    if (VerifVictoireLigne(player) || VerifVictoireColonne(player) || VerifVictoireDiagonal(player))
    {
        cout << VerifVictoireLigne(player) << " " << VerifVictoireColonne(player) << " " << VerifVictoireDiagonal(player) << endl;
        return true;
    }
    else
        return false;
}