#ifndef grille_h
#define grille_h

#include <iostream>
#include <string>
#include <vector>

class Grille{
    private:
        int longueur;
        int largeur;
        std::vector<std::vector<int>> grille;
    public:
        Grille();
        Grille(int width, int length);
        void AfficherGrille();
        std::vector<std::vector<int>> getGrille();
        void Remplir(int w, int l,int symb);
        int getLongueur();
        int getLargeur();

};
#endif