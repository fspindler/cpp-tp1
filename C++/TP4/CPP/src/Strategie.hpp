#ifndef STRATEGIE_H
#define STRATEGIE_H

#include <iostream>
#include "Grille.hpp"
#include "Player.hpp"
using namespace std;

class Strategie
{
protected:
    Grille grille;
    Player player1;
    Player player2;
    Player currentPlayer;

public:
    Strategie(){};
    virtual void Lance() = 0;
    virtual void Jeu()= 0;
    virtual void Tour(Player player) = 0;
    virtual bool VerifVictoire(Player player)=0;
    virtual bool VerifVictoireLigne(Player player) = 0;
    virtual bool VerifVictoireColonne(Player player) = 0;
    virtual bool VerifVictoireDiagonal(Player player) = 0;
    virtual bool Egalite()=0;
    virtual void InitJoueur()=0;
};
#endif