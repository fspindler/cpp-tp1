#include "Morpion.hpp"
#include "Strategie.hpp"
#include "Contexte.hpp"
#include "Puissance4.hpp"

int main(int argc, char **argv)
{

    Morpion morpion;
    Puissance4 puissance4;
    Contexte context;

    cout<< "A quelle jeu voulez vous jouer ? \n 1: Morpion \n 2:Puissance 4"<<endl;
    int response;
    cin >> response;
    switch (response)
    {
    case 1:
        context.set_strategy(&morpion);        
        break;
    case 2:
        context.set_strategy(&puissance4);
        break;
    default:
        cout<<"Euh ?"<<endl;
        break;
    }
    context.Lance();

}