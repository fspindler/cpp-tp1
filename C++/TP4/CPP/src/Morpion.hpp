#ifndef MORPION_H
#define MORPION_H
#include "Strategie.hpp"
#include "Grille.hpp"
#include <iostream>
#include <string>

class Morpion : public Strategie
{
private:
    Grille grille = Grille(3, 3);

public:
    Morpion();
    void Lance() override;
    bool VerifVictoireLigne(Player player) override;
    bool VerifVictoireColonne(Player player) override;
    bool VerifVictoireDiagonal(Player player) override;
    void Tour(Player player) override;
    void Jeu() override;
    void InitJoueur() override;
    bool Egalite() override;
    bool VerifVictoire(Player player) override;
    
};

#endif