#ifndef PUISSANCE4_H
#define PUISSANCE4_H
#include "Strategie.hpp"
#include "Grille.hpp"
#include <iostream>
#include <string>

class Puissance4 : public Strategie
{
private:
    Grille grille = Grille(4, 7);

public:
    Puissance4();
    void Lance() override;
    bool VerifVictoireLigne(Player player) override;
    bool VerifVictoireColonne(Player player) override;
    bool VerifVictoireDiagonal(Player player) override;
    void Tour(Player player) override;
    void Jeu() override;
    void InitJoueur() override;
    bool Egalite() override;
    bool VerifVictoire(Player player) override;
    
};

#endif