#ifndef player_h
#define player_h

#include <iostream>
#include <string>
#include <vector>

class Player
{
    private:
        std::string name;
        int symbole;
    public:
        Player();
        std::string getName();
        void setName(std::string name);
        int getSymbole();
        void setSymbole(int symbole);
};

#endif