#ifndef Contexte_h
#define Contexte_h

#include "Strategie.hpp"
#include <iostream>
#include <memory>


class Contexte
{
    private:
    Strategie* strategie;
    public:
        Contexte(){};
        ~Contexte()=default;
        void set_strategy(Strategie* strategie)
        {
            this->strategie = strategie;
        }
        void Lance()
        {
            strategie->Lance();
        }
};
#endif