#include "puissance.hpp"

Puissance::Puissance()
{
    this->board = new Board(7, 4);
    this->p1 = Player("Player 1", 'X');
    this->p2 = Player("Player 2", 'O');
}
Puissance::Puissance(Player p1, Player p2)
{
    this->board = new Board(3, 3);
    this->p1 = p1;
    this->p2 = p2;
}
void Puissance::Round(Player p) const
{
    int input_x;
    int input_y = 7;
    cout << "Choisez la colonne" << endl;
    cin >> input_x;
    for (int i = 0; i < 7; i++)
    {
        if (board->ElementAt(7, i) == '0')
        {
            if (input_y > i)
            {
                input_y = i;
            }
        }
    }
    board->AddSign(p.getSign(), input_x, input_y);
}

bool Puissance::CheckWin(Player p) const
{
    if (CheckWinByColumn(p) || CheckWinByRow(p) || CheckWinByDiagonal(p))
        return true;
    else
        return false;
}
bool Puissance::CheckWinByColumn(Player p) const
{
    int streak = 0;
    int x = board->getX();
    int y = board->getY();
    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            if (board->ElementAt(x, y) == p.getSign())
            {
                streak++;
                if (streak > 4)
                {
                    return true;
                }
            }
            else
                streak = 0;
        }
        streak = 0;
    }

    return false;
}
bool Puissance::CheckWinByRow(Player p) const
{
    int streak = 0;
    int x = board->getX();
    int y = board->getY();
    for (int i = 0; i < y; i++)
    {
        for (int j = 0; j < x; j++)
        {
            if (board->ElementAt(x, y) == p.getSign())
            {
                streak++;
                if (streak > 4)
                {
                    return true;
                }
            }
            else
                streak = 0;
        }
        streak = 0;
    }

    return false;
}
bool Puissance::CheckWinByDiagonal(Player p) const
{
    int streak = 0;
    int j = 0;
    for (int i = 0; i < 4; i++)
    {
        if (board->ElementAt(i, 0) == p.getSign())
        {
            for (j = 0; j < 4; j++)
            {
                if(board->ElementAt(i+1, j) == p.getSign())
                {
                    streak++;
                }
            }
        }
        if(streak == 4)
        {
            return true;
        }
        else
        {
            streak = 0;
        }
    }

       for (int i = 7; i > 4; i--)
    {
        if (board->ElementAt(i, 0) == p.getSign())
        {
            for (j = 0; j < 4; j++)
            {
                if(board->ElementAt(i-1, j) == p.getSign())
                {
                    streak++;
                }
            }
        }
        if(streak == 4)
        {
            return true;
        }
        else
        {
            streak = 0;
        }
    }

    return false;
}
bool Puissance::CheckEqual() const
{
    int equal = 0;
    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            if (board->ElementAt(i, j) == '0')
                equal++;
        }
    }
    if (equal == 28)
        return true;
    else
        return false;
}
void Puissance::Game() const
{
    Player p = p1;
    while (!CheckWin(p) || !CheckEqual())
    {
        board->ViewBoard();
        Round(p);
        p = p2;
    }
    board->ViewBoard();
    cout << "La partie est finie";
}
