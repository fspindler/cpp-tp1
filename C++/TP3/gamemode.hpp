#ifndef GAMEMODE_HPP
#define GAMEMODE_HPP
#include "board.hpp"
#include "player.hpp"

class Gamemode
{

    public:
        Gamemode();
        virtual bool CheckWin(Player p) const = 0;
        virtual bool CheckWinByRow(Player p) const = 0;
        virtual bool CheckWinByColumn(Player p) const = 0;
        virtual bool CheckWinByDiagonal(Player p) const = 0;
        virtual bool CheckEqual() const = 0;
        virtual void Round(Player p) const = 0;
        virtual void Game() const = 0;

    private:
        Board* board;
        Player p1;
        Player p2;
};


#endif // GAMEMODE_HPP