#include "morpion.hpp"

Morpion::Morpion()
{
    this->board = new Board(3, 3);
    this->p1 = Player("Player 1",'X');
    this->p2 = Player("Player 2",'O');
}
Morpion::Morpion(Player p1, Player p2)
{
    this->board = new Board(3, 3);
    this->p1 = p1;
    this->p2 = p2;
}
void Morpion::Round(Player p) const
{
    int input_x;
    int input_y;
    cout << "Choisez les coordonnées d'une case (X)" << endl;
    cin >> input_x;
    cout << "Choisez les coordonnées d'une case (Y)" << endl;
    cin >> input_y;
    board->AddSign(p.getSign(), input_x, input_y);
}

bool Morpion::CheckWin(Player p) const
{
    if (CheckWinByColumn(p) || CheckWinByRow(p) || CheckWinByDiagonal(p))
        return true;
    else
        return false;
}
bool Morpion::CheckWinByColumn(Player p) const
{
    int streak = 0;
    int x = board->getX();
    int y = board->getY();
    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            if (board->ElementAt(x, y) == p.getSign())
                streak++;
        }
    }
    if (streak == 3)
        return true;
    else
        return false;
}
bool Morpion::CheckWinByRow(Player p) const
{
    int streak = 0;
    int x = board->getX();
    int y = board->getY();
    for (int i = 0; i < y; i++)
    {
        for (int j = 0; j < x; j++)
        {
            if (board->ElementAt(x, y) == p.getSign())
                streak++;
        }
    }
    if (streak == 3)
        return true;
    else
        return false;
}
bool Morpion::CheckWinByDiagonal(Player p) const
{
    if (board->ElementAt(0, 0) == p.getSign() || board->ElementAt(1, 1) == p.getSign() || board->ElementAt(2, 2) == p.getSign())
    {
        return true;
    }
    else if (board->ElementAt(0, 2) == p.getSign() || board->ElementAt(1, 1) == p.getSign() || board->ElementAt(2, 0) == p.getSign())
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool Morpion::CheckEqual() const
{
    int equal = 0;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if(board->ElementAt(i, j) == '0')
                equal++;
        }
    }
    if(equal == 9)
        return true;
    else
        return false;    
}
void Morpion::Game() const
{
    Player p = p1;
    while(!CheckWin(p) || !CheckEqual())
    {
        board->ViewBoard();
        Round(p);
        p = p2;
    }
    board->ViewBoard();
    cout << "La partie est finie";
}