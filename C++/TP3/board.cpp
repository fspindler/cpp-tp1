#include "board.hpp"

Board::Board(int x , int y )
{
    board.resize(x);
    for (int i = 0; i < x; i++)
    {
        board[i].resize(y);
        for (int j = 0; j < y;j++)
        {
            board[i][j] = '0';
        }
    }
}

void Board::AddSign(char sign, int x, int y)
{
    board[x][y] = sign;
}
void Board::ViewBoard()
{
    for (long unsigned int i = 0; i < board.size(); i++)
    {
        for (long unsigned int j = 0; j < board[i].size(); j++)
        {
            cout << board[i][j] << " ";
        }
        cout << endl;
    }
}
char Board::ElementAt(int x, int y)
{
    return board[x][y];
}
