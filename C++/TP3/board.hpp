#ifndef BOARD_H
#define BOARD_H
#include "default.hpp"
class Board
{
private:
    vector <vector<char> > board;
    int x;
    int y;
public:
    Board(int x , int y );
    void ViewBoard();
    void AddSign(char sign, int x, int y);
    char ElementAt(int x, int y);
    inline int getX() { return x; }
    inline int getY() { return y; }

};

#endif // BOARD_H