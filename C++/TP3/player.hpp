#ifndef PLAYER_H
#define PLAYER_H
#include "default.hpp"
class Player
{
private:
    string name;
    char sign;

public:
    Player();
    Player(string name,char sign);
    inline string getName() const
    {
        return name;
    }
    inline char getSign() const
    {
        return sign;
    }
};

#endif // PLAYER_H