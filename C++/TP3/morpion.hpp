#ifndef MORPION_HPP
#define MORPION_HPP

#include "gamemode.hpp"

class Morpion : public Gamemode
{
public:
    Morpion();
    Morpion(Player p1, Player p2);
    bool CheckWin(Player p) const override;
    bool CheckWinByRow(Player p) const override;
    bool CheckWinByColumn(Player p) const override;
    bool CheckWinByDiagonal(Player p) const override;
    bool CheckEqual() const override;
    void Round(Player p) const override;
    void Game() const override;

private:
    Board *board;
    Player p1;
    Player p2;
};

#endif