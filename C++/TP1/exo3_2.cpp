#include <unistd.h>
#include <iostream>
#include <random>
#include <string>

int main()
{
    srand(time(nullptr));
    bool t = true;
    int i = rand() % 1001;
    int j;
    int compteur = 0;
    printf("Devine le nombre\n");
    while (t)
    {
        compteur++;
        scanf("%d", &j);
        if (j == i)
        {
            printf("Bien joué, tu as réussi en %d essais\n",compteur);
            t = false;
        }
        else if (j > i)
        {
            printf("Moins\n");
        }
        else
        {
            printf("Plus\n");
        }
    }
}