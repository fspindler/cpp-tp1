#include <unistd.h>
#include <iostream>
#include <random>

void score(int p1, int p2)
{
    if (p1 < 4 && p2 < 4)
    {
        switch (p1)
        {
        case 0:
            p1 = 0;
            break;
        case 1:
            p1 = 15;
            break;
        case 2:
            p1 = 30;
            break;
        case 3:
            p1 = 40;
            break;
        }
        switch (p2)
        {
        case 0:
            p2 = 0;
            break;
        case 1:
            p2 = 15;
            break;
        case 2:
            p2 = 30;
            break;
        case 3:
            p2 = 40;
            break;
        }
        printf("Joueur 1 : %d | Joueur 2 : %d\n", p1, p2);
    }
    else
    {
        if (p1 - p2 >= 2)
        {
            printf("Joueur 1 est le grand(e) gagnant(e) \n");
        }
        else if (p2 - p1 >= 2)
        {
            printf("Joueur 2 est le grand(e) gagnant(e) \n");
        }
        else if (p1 - p2 == 1)
        {
            printf("Joueur 1 : AVT | Joueur 2 : 40\n");
        }
        else if (p2 - p1 == 1)
        {
            printf("Joueur 1 : 40 | Joueur 2 : AVT\n");
        }
        else
        {
            printf("Joueur 1 : 40 | Joueur 2 : 40\n");
        }
    }
}

int main()
{
    int p1, p2;
    
    printf("Nombre d'échange gagner par le Joueur 1 : ");
    scanf("%d", &p1);
    printf("\n");
    printf("Nombre d'échange gagner par le Joueur 2 : ");
    scanf("%d", &p2);
    printf("\n");
    score(p1, p2);
}