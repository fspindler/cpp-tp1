#include <unistd.h>
#include <iostream>
#include <random>
#include <string>

int main()
{
    std::string nom;
    std::string prenom;
    printf("C'est quoi ton petit nom et prénom (nom prénom)\n");
    std::cin >> nom >> prenom;
    for(int i=0; i< (int)nom.size(); i++)
    {
        nom[i] = toupper(nom[i]);
    }
    prenom[0] = toupper(prenom[0]);
    std::cout << "Nom : " << nom << " Prénom : " << prenom << "\n";
}