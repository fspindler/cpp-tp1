#include <unistd.h>
#include <iostream>

int somme(int a, int b)
{
    return a + b;
}
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void swap(int *a, int *b, int *c)
{
    *c = somme(*a, *b);
}
void swap(int a, int b, int &r)
{
    r = somme(a, b);
}

int main()
{
    int a = 2;
    int b = 4;
    int c;
    swap(a, b, c);
    printf("A:%d  B:%d C:%d \n", a, b, c);
}
