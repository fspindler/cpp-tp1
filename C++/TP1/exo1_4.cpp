#include <unistd.h>
#include <iostream>
#include <random>

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sortcroissant(int tab[], int size)
{
    for (int i = 0; i < size -1 ; ++i)
    {
        for (int j = 0; j < size - i -1 ; ++j)
        {
            if (tab[j] > tab[j + 1])
            {
                swap(&tab[j], &tab[j + 1]);
            }
        }
    }
}

void sortdecroissant(int tab[], int size)
{
    for (int i = 0; i < size -1; ++i)
    {
        for (int j = 0; j < size - i - 1; ++j)
        {
            if (tab[j] < tab[j + 1])
            {
                swap(&tab[j], &tab[j + 1]);
            }
        }
    }
}
void affichtab(int tab[], int size)
{
    printf("*********************************\n");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

int main()
{
    srand(time(nullptr));
    int sort;
    int size = 10;
    printf("Choisie une taille de tableau\n");
    scanf("%d", &size);
    printf("Trie croissant 0 et trie décroissant 1 \n");
    scanf("%d", &sort);
    int tab[size];
    for (int i = 0; i < size; i++)
    {
        // Valeur entre 1 et 100 pour plus de clartée
        tab[i] = rand() % 100 + 1;
    }
    if (sort == 1)
    {
        affichtab(tab, size);
        sortdecroissant(tab, size);
        affichtab(tab, size);
        sortcroissant(tab, size);
        affichtab(tab, size);
    }
    else
    {
        affichtab(tab, size);
        sortcroissant(tab, size);
        affichtab(tab, size);
        sortdecroissant(tab, size);
        affichtab(tab, size);
    }
}
