#ifndef DEFAULT_H
#define DEFAULT_H
#include <unistd.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

struct Point
{
    float x, y;
};

#endif // DEFAULT_H