#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "default.hpp"

class Rectangle
{
private:
    int Longueur;
    int Largeur;
    Point HG;

public:
    Rectangle(const int longueur, const int largeur, Point hg);
    void Afficher() const;
    inline int getLongueur() const;
    inline void setLongueur(const int longueur);
    inline int getLargeur() const;
    inline void setLargeur(const int largeur);
    inline int Perimetre() const;
    inline int Surface() const;
    inline bool PlusGrandPerimetre(const Rectangle &rectangle) const;
    inline bool PlusGrandeSurface(const Rectangle &rectangle) const;
};
#endif // RECTANGLE_H
