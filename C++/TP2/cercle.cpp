#include "cercle.hpp"

Cercle::Cercle(const Point &centre, int diametre)
{
    Centre = centre;
    Diametre = diametre;
}
// Méthode qui renvoie le centre du Cercle
Point Cercle ::getCentre() const
{
    return Centre;
}
// Méthode qui set le centre du Cercle
void Cercle ::setCentre(const Point &centre)
{
    Centre = centre;
}
// Méthode qui renvoie le diamètre du Cercle
int Cercle ::getDiametre() const
{
    return Diametre;
}
// Méthode qui set le diamètre du Cercle
void Cercle ::setDiametre(int diametre)
{
    Diametre = diametre;
}
// Méthode qui calcule la surface du Cercle
float Cercle ::Surface() const
{
    return M_PI * Diametre / 2.0 * Diametre / 2.0;
}
// Méthode qui calcule le périmetre du Cercle
float Cercle ::Perimetre() const
{
    return M_PI * Diametre;
}
// Méthode qui détermine pour un Point s'il est comprit sur le périmètre du Cercle
// Si la distance qui sépare le centre du Cercle et le Point est égale au Rayon alors renvoie true
bool Cercle ::SurCercle(const Point &p) const
{
    return sqrt((p.x - Centre.x) * (p.x - Centre.x) + (p.y - Centre.y) * (p.y - Centre.y)) == Diametre / 2;
}
// Méthode qui détermine pour un Point s'il est comprit dans le Cercle
// Si la distance qui sépare le centre du Cercle et le Point est inférieur au Rayon alors renvoie true
bool Cercle ::DansCercle(const Point &p) const
{
    return sqrt((p.x - Centre.x) * (p.x - Centre.x) + (p.y - Centre.y) * (p.y - Centre.y)) < Diametre / 2;
}
void Cercle::Afficher() const
{
    cout << "Cercle" << endl
         << "Centre : x = " << getCentre().x << " y = " << getCentre().y << endl
         << "Diamètre : " << getDiametre() << endl
         << "Surface : " << Surface() << endl
         << "Périmetre : " << Perimetre() << endl;
}
int main()
{
    Point c{0, 0};
    Cercle cercle(c, 4);
    cercle.Afficher();
}