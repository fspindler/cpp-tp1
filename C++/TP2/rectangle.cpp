#include "rectangle.hpp"

// Méthode qui compare les surfaces de deux Rectangle
bool Rectangle ::PlusGrandeSurface(const Rectangle &rectangle) const
{
    return Surface() > rectangle.Surface();
}
// Méthode qui compare les périmetres de deux Rectangle
bool Rectangle ::PlusGrandPerimetre(const Rectangle &rectangle) const
{
    return Perimetre() > rectangle.Perimetre();
}
// Méthode qui calcule la surface d'un rectangle
int Rectangle ::Surface() const
{
    return Largeur * Longueur;
}
// Méthode qui calcule le périmetre d'un Rectangle
int Rectangle ::Perimetre() const
{
    return (Largeur + Longueur) * 2;
}
// Méthode qui permetre de set la largeur d'un Rectangle
void Rectangle ::setLargeur(const int largeur)
{
    Largeur = largeur;
}
// Méthode qui permetre de renvoyer la largeur d'un Rectangle
int Rectangle ::getLargeur() const
{
    return Largeur;
}
// Méthode qui permetre de set la longueur d'un Rectangle
void Rectangle ::setLongueur(const int longueur)
{
    Longueur = longueur;
}
// Méthode qui permetre de renvoyer la longueur d'un rectangle
int Rectangle ::getLongueur() const
{
    return Longueur;
}
// Méthode qui affiche des infos sur le Rectangle
void Rectangle ::Afficher() const
{
    cout << "Rectangle" << endl
         << "Longueur : " << getLongueur() << endl
         << "Largeur : " << getLargeur() << endl
         << "Périmetre : " << Perimetre() << endl
         << "Surface : " << Surface() << endl;
}
// Constructeur
Rectangle ::Rectangle(const int longueur, const int largeur, Point hg)
{
    Longueur = longueur;
    Largeur = largeur;
    HG = hg;
}
int main()
{
    Point p{0, 0};
    Rectangle rectangle(3, 2, p);
    rectangle.getLargeur();
    rectangle.Afficher();
}
