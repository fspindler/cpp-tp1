#include "triangle.hpp"

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
}
// Méthode définie le point 1
void Triangle ::set_p1(const Point &p)
{
    this->p1 = p;
}
// Méthode définie le point 2
void Triangle ::set_p2(const Point &p)
{
    this->p2 = p;
}
// Méthode définie le point 3
void Triangle ::set_p3(const Point &p)
{
    this->p3 = p;
}
// Méthode return le point 1
Point Triangle ::get_p1() const
{
    return this->p1;
}
// Méthode return le point 2
Point Triangle ::get_p2() const
{
    return this->p2;
}
// Méthode return le point 3
Point Triangle ::get_p3() const
{
    return this->p3;
}
// Méthode qui calcule la longueur de la base
// Il suffit de récupérer la longueurs la plus grandes
float Triangle ::Base() const
{
    vector<float> longueurs = this->Longueurs();
    return *max_element(longueurs.begin(), longueurs.end());
}
// Méthode qui calcule la Hauteur
// Pour cela on utilise la formule 2*Surface/Base
float Triangle ::Hauteur() const
{
    return 2.0 * this->Surface() / this->Base();
}
// Méthode qui calcule la surface
// Pour cela l'on utilise la formule de Héron
float Triangle ::Surface() const
{
    vector<float> longueurs = this->Longueurs();
    float p = (longueurs[0] + longueurs[1] + longueurs[2]) / 2.0;
    return sqrt(p * (p - longueurs[0]) * (p - longueurs[1]) * (p - longueurs[2]));
}
// Méthode qui calcule la longueur des segments du Triangle
vector<float> Triangle ::Longueurs() const
{
    vector<float> longueurs;
    float A = sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
    float C = sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y));
    float B = sqrt((p3.x - p1.x) * (p3.x - p1.x) + (p3.y - p1.y) * (p3.y - p1.y));
    longueurs.push_back(A);
    longueurs.push_back(B);
    longueurs.push_back(C);
    return longueurs;
}
// Méthode qui détermine si un triangle est isocèle
bool Triangle ::isIsocele() const
{
    vector<float> longueurs = Longueurs();
    if (longueurs[0] == longueurs[1] || longueurs[0] == longueurs[2] || longueurs[1] == longueurs[2])
    {
        return true;
    }
    else
    {
        return false;
    }
}
// Méthode qui détermine si un triangle est equilatéral
bool Triangle ::isEquilateral() const
{
    vector<float> longueurs = Longueurs();
    if (longueurs[0] == longueurs[1] && longueurs[0] == longueurs[2] && longueurs[1] == longueurs[2])
    {
        return true;
    }
    else
    {
        return false;
    }
}
// Méthode qui détermine si un triangle est rectangle
// Grâce au théorème de pythagore
bool Triangle ::isRectangle() const
{
    vector<float> longueurs = Longueurs();
    // On arrondit les floats pour éviter tout problème durant les comparaisons
    float A = ceil(pow(longueurs[0], 2) * 10000.0 - 0.5) / 1000.0;
    float B = ceil(pow(longueurs[1], 2) * 10000.0 - 0.5) / 1000.0;
    float C = ceil(pow(longueurs[2], 2) * 10000.0 - 0.5) / 1000.0;
    if (A == B + C)
        return true;
    else if (B == A + C)
        return true;
    else if (C == B + A)
        return true;
    else
        return false;
}
void Triangle ::Afficher() const
{
    cout << "Triangle" << endl
         << "Point 1 : x = " << get_p1().x << " y = " << get_p1().y << endl
         << "Point 2 : x = " << get_p2().x << " y = " << get_p2().y << endl
         << "Point 1 : x = " << get_p3().x << " y = " << get_p3().y << endl
         << "Base : " << Base() << endl
         << "Hauteur: " << Hauteur() << endl
         << "Surface : " << Surface() << endl
         << "Longueur : A =" << Longueurs()[0] << " B = " << Longueurs()[1] << " C = " << Longueurs()[2] << endl
         << "Isocele : " << isIsocele() << endl
         << "Equilatéral : " << isEquilateral() << endl
         << "Rectangle : " << isRectangle() << endl;
}
int main()
{
    Point p1{0, 0};
    Point p2{0, 2};
    Point p3{2, 0};
    Triangle triangle(p1, p2, p3);
    triangle.Afficher();
}
