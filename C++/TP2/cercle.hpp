#ifndef CERCLE_H
#define CERCLE_H
#include "default.hpp"

class Cercle
{
private:
    Point Centre;
    int Diametre;

public:
    Cercle(const Point& centre, int diametre);

    inline Point getCentre() const;
    inline void setCentre(const Point &centre);
    inline int getDiametre() const;
    inline void setDiametre(int diametre);
    inline float Surface() const;
    inline float Perimetre() const;
    inline bool SurCercle(const Point& p) const;
    inline bool DansCercle(const Point& p) const;
    void Afficher() const;
};

#endif // CERCLE_H