#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "default.hpp"

class Triangle
{
private:
    Point p1;
    Point p2;
    Point p3;

public:
    Triangle(const Point& p1,const Point& p2,const Point& p3);

    inline void set_p1(const Point &p);
    inline void set_p2(const Point &p);
    inline void set_p3(const Point &p);
    inline Point get_p1() const;
    inline Point get_p2() const;
    inline Point get_p3() const;
    float Base() const;
    inline float Hauteur() const;
    float Surface() const;
    vector<float> Longueurs() const;
    bool isIsocele() const;
    bool isEquilateral() const;
    bool isRectangle() const;
    void Afficher()const;
};

#endif // TRIANGLE_H